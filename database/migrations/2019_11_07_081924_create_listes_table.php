<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listes', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('codeListe');
            $table->integer('Quantite');
            $table->boolean('Traite')->default(false);
            $table->boolean('Receptionne')->default(false);
            $table->unsignedBigInteger('agence_depart');
            $table->unsignedBigInteger('agence_arrivee');
            $table->unsignedBigInteger('transport_type_id');

            $table->foreign('agence_depart','liste_expedition')
            ->references('id')
            ->on('agences')
            ->onDelete('restrict');

            $table->foreign('agence_arrivee','liste_reception')
            ->references('id')
            ->on('agences')
            ->onDelete('restrict');

            $table->foreign('transport_type_id','liste_transport')
            ->references('id')
            ->on('transport_types')
            ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listes');
    }
}
