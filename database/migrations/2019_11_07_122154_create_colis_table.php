<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('Quantite');
            $table->string('Detail');
            $table->decimal('Longueur')->nullable();
            $table->decimal('Largeur')->nullable();
            $table->decimal('Hauteur')->nullable();
            $table->decimal('Masse')->nullable();
            $table->unsignedBigInteger('lot_id');

            $table->foreign('lot_id','colis_lot')
                ->references('id')
                ->on('lots')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colis');
    }
}
