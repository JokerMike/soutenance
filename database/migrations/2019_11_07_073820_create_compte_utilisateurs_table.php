<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompteUtilisateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compte_utilisateurs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Pseudonyme');
            $table->string('Telephone');
            $table->string('Password');
            $table->string('Code_pays');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compte_utilisateurs');
    }
}
