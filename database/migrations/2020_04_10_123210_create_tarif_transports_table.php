<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTarifTransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarif_transports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transport_id');
            $table->unsignedBigInteger('tarif_id');

            $table->foreign('transport_id')->references('id')->on('transport_types')
                ->onDelete('cascade');

            $table->foreign('tarif_id')->references('id')->on('tarifs')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarif_transports');
    }
}
