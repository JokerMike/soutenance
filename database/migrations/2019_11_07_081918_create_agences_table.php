<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Nom');
            $table->string('Telephone');
            $table->unsignedBigInteger('localisation_id');
            $table->unsignedBigInteger('agent_chef');

            $table->foreign('localisation_id','localisation_agence')
                ->references('id')
                ->on('villes')
                ->onDelete('restrict');
            $table->foreign('agent_chef','agence_responsable')
                ->references('id')
                ->on('agences')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agences');
    }
}
