<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('Quantite');
            $table->integer('numero_track')->nullable();
            $table->boolean('traite')->default(false);
            $table->string('statut')->default('enregistrement incomplet');
            $table->unsignedBigInteger('expediteur')->nullable();
            $table->unsignedBigInteger('destinataire')->nullable();
            $table->unsignedBigInteger('liste_id')->nullable();
            $table->unsignedBigInteger('point_id');
            $table->unsignedBigInteger('agence_id');
            $table->unsignedBigInteger('itineraire_id');
            $table->unsignedBigInteger('livraison_id')->nullable();

            $table->foreign('expediteur','lot_expediteur')
            ->references('id')
            ->on('utilisateurs')
            ->onDelete('cascade');


            $table->foreign('agence_id','agence_lot')
            ->references('id')
            ->on('agences')
            ->onDelete('cascade');

            $table->foreign('point_id','pointRetrait_lot')
            ->references('id')
            ->on('point_retraits')
            ->onDelete('cascade');

            $table->foreign('destinataire','lot_destinataire')
            ->references('id')
            ->on('utilisateurs')
            ->onDelete('cascade');

            $table->foreign('liste_id','lot_liste')
            ->references('id')
            ->on('listes')
            ->onDelete('cascade');

            $table->foreign('livraison_id','lot_livraison')
            ->references('id')
            ->on('livraisons')
            ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lots');
    }
}
