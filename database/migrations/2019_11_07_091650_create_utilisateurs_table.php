<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtilisateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utilisateurs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Nom');
            $table->string('Prenom');
            $table->string('Telephone');
            $table->string('Email')->nullable();
            $table->string('BP')->nullable();
            $table->unsignedBigInteger('compte_utilisateur')->nullable();
            $table->unsignedBigInteger('administrateur')->nullable();
            $table->unsignedBigInteger('agent')->nullable();

            $table->foreign('compte_utilisateur','utilisateur_compte')
            ->references('id')
            ->on('compte_utilisateurs')
            ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utilisateurs');
    }
}
