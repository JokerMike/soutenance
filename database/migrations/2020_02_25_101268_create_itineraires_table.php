<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinerairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itineraires', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ville_depart');
            $table->unsignedBigInteger('ville_destination');
            $table->unsignedBigInteger('agences_id');

            $table->foreign('ville_depart','ville_depart_itineraire')
            ->references('id')
            ->on('villes')
            ->onDelete('cascade');

            $table->foreign('ville_destination','ville_destination_itineraire')
            ->references('id')
            ->on('villes')
            ->onDelete('cascade');

            $table->foreign('agences_id','agence_itineraire')
            ->references('id')
            ->on('agences')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itineraires');
    }
}
