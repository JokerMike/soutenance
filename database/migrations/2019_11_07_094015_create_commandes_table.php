<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Article');
            $table->integer('Quantite');
            $table->string('Lien_commande');
            $table->unsignedBigInteger('agent_id');
            $table->unsignedBigInteger('utilisateur_id');

            $table->foreign('agent_id','commande_agent')
            ->references('id')
            ->on('agents')
            ->onDelete('restrict');

            $table->foreign('utilisateur_id','commande_utilisateur')
            ->references('id')
            ->on('utilisateurs')
            ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes');
    }
}
