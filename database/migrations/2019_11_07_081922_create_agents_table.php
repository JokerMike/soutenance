<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('Date_embauche');
            $table->unsignedBigInteger('agence_id')->nullable();
            $table->unsignedBigInteger('responsable_agence')->nullable();
            ;
            $table->foreign('agence_id','agent_agence')
            ->references('id')
            ->on('agences')
            ->onDelete('restrict');
            $table->foreign('responsable_agence','agence_chef')
                ->references('id')
                ->on('agences')
                ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
