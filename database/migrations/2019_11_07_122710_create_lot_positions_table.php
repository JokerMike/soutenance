<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_positions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('lots_id');
            $table->unsignedBigInteger('positions_id');

            $table->foreign('lots_id','lot_position_lot')
            ->references('id')
            ->on('lots')
            ->onDelete('cascade');

            $table->foreign('positions_id','lot_position_position')
            ->references('id')
            ->on('positions')
            ->onDelete('cascade');
            $table->timestamps();
        });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_positions');
    }
}
