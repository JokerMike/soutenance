<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalisationUtilisateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localisation_utilisateurs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ville_id');
            $table->unsignedBigInteger('utilisateurs_id');

            $table->foreign('ville_id','localisation_utilisateur_ville')
            ->references('id')
            ->on('villes')
            ->onDelete('cascade');

            $table->foreign('utilisateurs_id','localisation_utilisateur_utilisateur')
            ->references('id')
            ->on('utilisateurs')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localisation_utilisateurs');
    }
}
