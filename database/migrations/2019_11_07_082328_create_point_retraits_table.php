<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointRetraitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_retraits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->text('adresse');
            $table->string('telephone');
            $table->unsignedBigInteger('responsable');
            $table->unsignedBigInteger('localisation_id');
            $table->unsignedBigInteger('agence_id');
            
            $table->foreign('localisation_id','localisation_point')
                ->references('id')
                ->on('villes')
                ->onDelete('restrict');

            $table->foreign('agence_id','point_retrait_agence')
            ->references('id')
            ->on('agences')
            ->onDelete('restrict');

            $table->foreign('responsable','point_retrait_responsable')
            ->references('id')
            ->on('agents')
            ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_retraits');
    }
}
