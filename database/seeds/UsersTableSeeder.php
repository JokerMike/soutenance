<?php

use Illuminate\Database\Seeder;
use App\Models\compteUtilisateurs;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $compte = new compteUtilisateurs;
        $compte->Telephone = "+22890301399";
        $compte->Pseudonyme = "mike";
        $compte->Password = Hash::make("webmaster98");
        $compte->Code_pays = "TG";
        $compte->assignRole("admin");
        $compte->save();

    }
}
