<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class Role1TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = "user";
        $role->guard_name = "web";
        $role->syncPermissions("colis-retirer");
        $role->save();
    }
}
