export let routes = [

    { path: '/liste', component: require('./components/List.vue').default },
    { path: '/nouvelEnvoi', component: require('./components/envois/nouvelEnvoi.vue').default },
    { path: '/mon-compte', component: require('./components/myAccount.vue').default },
    { path: '/pdf/:id', component: require('./components/pdf.vue').default },
    { path: '/detail/lot/:id', component: require('./components/envois/detailEnvoi.vue').default },
    { path: '/accueil', component: require('./components/Accueil.vue').default },
    { path: '/enregistrement', component: require('./components/enregistrements/Enregistrement.vue').default },
    { path: '/form', component: require('./components/Form.vue').default },
    { path: '/completeInfo/lot/:id', component: require('./components/enregistrements/ajoutInfo.vue').default },
    { path: '/agence', component: require('./components/Agence/agence.vue').default },
    { path: '/newTarif', component: require('./components/Agence/newTarif.vue').default },
    { path: '/tarif_table', component: require('./components/Agence/tarif_table.vue').default },

    { path: '/updateTarif/:id', component: require('./components/Agence/updateTarif.vue').default },

    { path: '/ajout_point', component: require('./components/Agence/point_retrait_ajout.vue').default },
    { path: '/ajoutEmploye', component: require('./components/Agence/newEmploye.vue').default },
    { path: '/AgenceManagement', component: require('./components/Administrator/AgenceManagement.vue').default },
    { path: '/ajoutAgence', component: require('./components/Administrator/AjoutAgence.vue').default },
    { path: '/Profils', component: require('./components/Administrator/Profils.vue').default },
    { path: '/AgencyEmployeList', component: require('./components/Agence/Employes.vue').default },
    { path: '/AllEmployeList', component: require('./components/Administrator/Employes.vue').default },
    { path: '/wel', component: require('./components/ExampleComponent.vue').default },
    { path: '/passport-clients', component: require('./components/passport/Clients.vue').default },
    { path: '/passport-authorized-clients', component: require('./components/passport/AuthorizedClients.vue').default },
    { path: '/passport-personal-access-tokens', component: require('./components/passport/PersonalAccessTokens.vue').default },

];