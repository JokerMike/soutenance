import Vue from 'vue';
import VueRouter from 'vue-router';
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import VueFormWizard from 'vue-form-wizard'
import Vuelidate from 'vuelidate'
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import VueQRCodeComponent from 'vue-qrcode-component';
import { routes } from "./Route"
import Vuex from 'vuex'
import Swal from 'sweetalert2'
import moment from 'moment';
import { Form, HasError, AlertError } from 'vform'
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.component('qr-code', VueQRCodeComponent)
require('./bootstrap');
window.Form = Form;
window.Swal = Swal
Vue.use(Vuex);

//const store = new Vuex.Store(StoreData);

window.Fire = new Vue();
window.Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
})
Vue.filter('lotTraite', function(value) {
    if (value == 0) {
        return "Non"
    } else {
        return "oui"
    }
});

Vue.filter('renseignement', function(value) {
    if (value == null) {
        return "Non renseigné"
    } else {
        return value
    }
})
Vue.filter('myDate', function(created) {
    return moment(created).format('L');
});

Vue.use(Loading, {
    color: '#ffffff',
    loader: 'Bars',
    width: 74,
    height: 74,
    backgroundColor: '#4b4b4b',
    opacity: 0.5,
    zIndex: 999,
    canCancel: true,
})

Vue.use(VueFormWizard)
Vue.use(Vuelidate)
Vue.use(VueRouter);


const router = new VueRouter({
    routes: routes,
    mode: 'history'
});
const app = new Vue({
    el: '#app',
    router,
    data: {
        search: '',

    },
    methods: {
        chef: _.debounce(() => {
            Fire.$emit('searching');

        }, 0),

        filter: _.debounce(() => {
            Fire.$emit('searchCities');

        }, 200)
    }

    // store,
});
