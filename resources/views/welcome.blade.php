<!DOCTYPE html>
<html lang="en">
    @include('layouts.partials._welcomeHeader')
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
    <div id="overlayer"></div>
    <div class="loader">
      <div class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>

    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>
      <div class="top-bar">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <a href="#" class=""><span class="mr-2  icon-envelope-open-o"></span> <span class="d-none d-md-inline-block">info@yourdomain.com</span></a>
              <span class="mx-md-2 d-inline-block"></span>
              <a href="#" class=""><span class="mr-2  icon-phone"></span> <span class="d-none d-md-inline-block">1+ (234) 5678 9101</span></a>


              <div class="float-right">

                <a href="#" class=""><span class="mr-2  icon-twitter"></span> <span class="d-none d-md-inline-block">Twitter</span></a>
                <span class="mx-md-2 d-inline-block"></span>
                <a href="#" class=""><span class="mr-2  icon-facebook"></span> <span class="d-none d-md-inline-block">Facebook</span></a>

              </div>

            </div>

          </div>

        </div>
      </div>

        <div id="app">
            <header class="site-navbar js-sticky-header site-navbar-target" role="banner">
                <div class="container">
                <div class="row align-items-center position-relative">
                    <div class="site-logo">
                    <a href="#" class="text-black"><span class="orange">Cargo</a>
                    </div>
                    <nav class="site-navigation text-right ml-auto " role="navigation">

                        <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                        <li>
                            <router-link to="/wel" class="nav-link ">
                                <span class="app-menu__label">Accueil</span>
                            </router-link>
                        </li>


                        <li><a href="#services-section" class="nav-link">Services</a></li>
                        <li class="has-children">
                            <a href="#about-section" class="nav-link">About Us</a>
                            <ul class="dropdown arrow-top">
                            <li><a href="#team-section" class="nav-link">Team</a></li>
                            <li><a href="#pricing-section" class="nav-link">Pricing</a></li>
                            <li><a href="#faq-section" class="nav-link">FAQ</a></li>
                            <li class="has-children">
                                <a href="#">More Links</a>
                                <ul class="dropdown">
                                <li><a href="#">Menu One</a></li>
                                <li><a href="#">Menu Two</a></li>
                                <li><a href="#">Menu Three</a></li>
                                </ul>
                            </li>
                            </ul>
                        </li>
                       {{-- @role('admin') --}}
                        <li>
                            <router-link to="/nouvelEnvoi" class="nav-link ">
                                <span class="app-menu__label">Envoi</span>
                            </router-link>
                        </li>
                        {{--@endrole--}}




                        @if (Route::has('login'))
                                @auth
                                <li>

                                    <a href="#testimonials-section" class="nav-link"><i class="fas fa-bell "></i>Notifications</a>
                                </li>
                                <span class="green">Connected as {{ Auth::user()->Pseudonyme }}</span>
                                <li>
                                        <a class="app-menu__item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                            <i class="app-menu__icon fas fa-power-off red"></i>
                                            <span class="app-menu__label red">Déconnexion</span>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                        </form>
                                </li>
                                @else
                                <li><a href="#GetOfferModal" class="nav-link" data-toggle="modal">Se connecter</a></li>

                                    @if (Route::has('register'))
                                <li><a href="{{ route('register') }}">Créer un compte</a></li>
                                    @endif
                                @endauth

                            @endif
                        </ul>
                    </nav>

                </div>

                    <div class="toggle-button d-inline-block d-lg-none"><a href="#" class="site-menu-toggle py-5 js-menu-toggle text-black">
                        <span class="icon-menu h3"></span></a></div>

                </div>
            </header>
            <main class="app-content">
                <router-view></router-view>
            </main>
        </div>
        </div>

        <div id="GetOfferModal" class="modal fade modal-get-offer" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>Entrez vos paramètres de connexion</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form class="form" role="form" autocomplete="off" id="formGetOffer" novalidate="" method="POST" action="{{ route('login') }}">
                            @csrf
                                <div class="form-group">
                                    <input type="tel" id="Telephone" class="form-control @error('Telephone') is-invalid @enderror" name="Telephone"  placeholder="numéro de téléphone" value="{{ old('Telephone') }}" required autocomplete="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span> @enderror
                                </div>
                            <div class="form-group">

                                    <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder="mot de passe" name="password" required autocomplete="current-password"> @error('password')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span> @enderror

                            </div>

                            <div class="col-md-4 offset-4">
                                <button type="submit" class="btn btn-primary text-white " id="btnGetOffer">Se connecter</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-7">
                <h2 class="footer-heading mb-4">About Us</h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
              </div>
              <div class="col-md-4 ml-auto">
                <h2 class="footer-heading mb-4">Features</h2>
                <ul class="list-unstyled">
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Testimonials</a></li>
                  <li><a href="#">Terms of Service</a></li>
                  <li><a href="#">Privacy</a></li>
                  <li><a href="#">Contact Us</a></li>
                </ul>
              </div>

            </div>
          </div>
          <div class="col-md-4 ml-auto">

            <div class="mb-5">
              <h2 class="footer-heading mb-4">Subscribe to Newsletter</h2>
              <form action="#" method="post" class="footer-suscribe-form">
                <div class="input-group mb-3">
                  <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary text-white" type="button" id="button-addon2">Subscribe</button>
                  </div>
                </div>
            </div>


            <h2 class="footer-heading mb-4">Follow Us</h2>
            <a href="#about-section" class="smoothscroll pl-0 pr-3"><span class="icon-facebook"></span></a>
            <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
            <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
            <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
            </form>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
              <p class="copyright">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
            </div>
          </div>

        </div>
      </div>
    </footer>

    </div>

    @include('layouts.partials._welcomeFooter')
  </body>

</html>
