    <script src="accueil/js/jquery-3.3.1.min.js"></script>
    <script  src="{{ mix('assets/js/app.js') }}"></script>
    <script defer src="{{ asset('assets/js/main.js') }}"></script>

    @auth
        <script>
        var user = @json(auth()->user());
        </script>
    @endauth

