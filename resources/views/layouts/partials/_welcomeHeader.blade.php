
  <head>
    <title>Cargo &mdash; Website Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,700|Oswald:400,700" rel="stylesheet">
    <link rel="stylesheet" href="accueil/fonts/icomoon/style.css">
    <link rel="stylesheet" href="{{ mix('assets/css/app.css') }}">
    <link rel="stylesheet" href="accueil/css/bootstrap.min.css">
    <link rel="stylesheet" href="accueil/css/style.css">
    <link rel="stylesheet" href="accueil/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="accueil/css/owl.carousel.min.css">
    <link rel="stylesheet" href="accueil/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="accueil/css/aos.css">

  </head>
