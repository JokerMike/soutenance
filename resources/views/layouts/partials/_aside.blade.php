<aside class="app-sidebar">
        <div class="app-sidebar__user">
            <img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
          <div>
        <p class="app-sidebar__user-name">{{ Auth::user()->Pseudonyme }} </p>
            <p class="app-sidebar__user-designation">{{ Auth::user()->Telephone }}</p>
          </div>
        </div>
        <ul class="app-menu">
            <li>
                <router-link class="app-menu__item" to="/accueil">
                    <i class="app-menu__icon fas fa-home "></i>
                    <span class="app-menu__label">Accueil</span>
                </router-link>
            </li>


            <li class="treeview ">
                    <a class="app-menu__item" href="#" data-toggle="treeview">
                        <i class="app-menu__icon fas fa-tasks "></i>
                        <span class="app-menu__label">Transactions</span>
                        <i class="treeview-indicator fa fa-angle-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <router-link class="treeview-item" to="/nouvelEnvoi">
                                <i class="icon fas fa-circle"></i>Envoyer un colis
                            </router-link>
                        </li>
                        <li>
                            <router-link class="treeview-item" to="#">
                                <i class="icon fas fa-circle"></i>Passer une commande
                            </router-link>
                        </li>
                        <li>
                            <router-link class="treeview-item" to="/liste">
                                <i class="icon fas fa-circle"></i>Liste de mes activites
                            </router-link>
                        </li>

                    </ul>
            </li>


                <li>
                    <router-link class="app-menu__item" to="/enregistrement">
                        <i class="app-menu__icon fas fa-scroll "></i>
                        <span class="app-menu__label">Enregistrements</span>
                    </router-link>
                </li>
                {{--<li class="treeview ">
                    <a class="app-menu__item" href="#" data-toggle="treeview">
                        <i class="app-menu__icon fas fa-cogs blue"></i>
                        <span class="app-menu__label">Authentifications</span>
                        <i class="treeview-indicator fa fa-angle-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <router-link class="treeview-item" to="/passport-clients">
                                <i class="icon fas fa-circle"></i>Clients
                            </router-link>
                        </li>
                        <li>
                            <router-link class="treeview-item" to="/passport-authorized-clients">
                                <i class="icon fas fa-circle"></i>clients autorises
                            </router-link>
                        </li>
                        <li>
                            <router-link class="treeview-item" to="/passport-personal-access-tokens">
                                <i class="icon fas fa-circle"></i>acces personnels
                            </router-link>
                        </li>

                    </ul>
                </li>--}}

               {{-- <li>
                    <router-link class="app-menu__item" to="#scs">
                        <i class="app-menu__icon fas fa-box"></i>
                        <span class="app-menu__label">Enregistrer un lot</span>
                    </router-link>
                </li> --}}

                {{-- <li>
                    <router-link class="app-menu__item" to="/AllEmployeList">
                        <i class="app-menu__icon fas fa-users"></i>
                        <span class="app-menu__label">Liste des employés</span>
                    </router-link>
                </li> --}}
                <li>
                    <router-link class="app-menu__item" to="/agence">
                        <i class="app-menu__icon fas fa-store-alt"></i>
                        <span class="app-menu__label">Mon agence </span>
                    </router-link>
                </li>
                <li>
                    <router-link class="app-menu__item" to="/AgencyEmployeList">
                        <i class="app-menu__icon fas fa-id-badge"></i>
                        <span class="app-menu__label">employés de l'agence </span>
                    </router-link>
                </li>
                <li>
                    <router-link class="app-menu__item" to="/AgenceManagement">
                        <i class="app-menu__icon fas fa-place-of-worship"></i>
                        <span class="app-menu__label">Agences </span>
                    </router-link>
                </li>
                <li>
                    <router-link class="app-menu__item" to="/Profils">
                        <i class="app-menu__icon fas fa-place-of-worship"></i>
                        <span class="app-menu__label">Profils-Permissions</span>
                    </router-link>
                </li>
                <li>
                        <router-link class="app-menu__item" to="/mon-compte">
                            <i class="app-menu__icon fas fa-user-circle"></i>
                            <span class="app-menu__label">Mon compte</span>
                        </router-link>
                </li>
                <li>
                        <a class="app-menu__item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                            <i class="app-menu__icon fas fa-power-off red"></i>
                            <span class="app-menu__label">Déconnexion</span>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                        </form>
                </li>
        </ul>
      </aside>

