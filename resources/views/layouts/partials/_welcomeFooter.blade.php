
    <script src="accueil/js/jquery-3.3.1.min.js"></script>
    <script  src="{{ mix('assets/js/app.js') }}"></script>
    <script src="accueil/js/aos.js"></script>
    <script src="{{ asset('accueil/js/main.js') }}"></script>
    <script src="accueil/js/owl.carousel.min.js"></script>
    <script src="accueil/js/jquery.sticky.js"></script>
    <script src="accueil/js/jquery.waypoints.min.js"></script>
    <script src="accueil/js/jquery.animateNumber.min.js"></script>
    <script src="accueil/js/jquery.fancybox.min.js"></script>
    <script src="accueil/js/jquery.easing.1.3.js"></script>

    @auth
    <script>
    var user = @json(auth()->user());
    </script>
@endauth

@auth
@include('flashy::message')
@endauth
    <script src="">
        $("#btnGetOffer").click(function(event) {

            //Fetch form to apply custom Bootstrap validation
            var form = $("#formGetOffer")

            if (form[0].checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
            }

            form.addClass('was-validated');
        });

    </script>
