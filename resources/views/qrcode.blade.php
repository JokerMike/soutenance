<!DOCTYPE html>
<html>
<head>
    <title>Laravel QR Code Example</title>
</head>
<body>

<div class="text-center" style="margin-top: 50px;">
    <h3>Laravel QR Code Example</h3>

    @foreach ($lot as $l)
        for($i=0;$i<$l->Quantite;$i++){
            {!! QrCode::size(300)->generate($l['numero_track']); !!}
        }
    @endforeach

    <p>MyNotePaper</p>
</div>

</body>
</html>
