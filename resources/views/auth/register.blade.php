<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/register.css">
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="build/css/intlTelInput.css">

    <title>Créer votre compte</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-md-5 register-left">
                        <img src="assets/images/register.png"/>
                       <h3>Avez vous déjà un compte?</h3>
                       <p>Si vous avez déjà un compte,cliquez ci-dessous pour vous connecter</p>


                            {{-----------------" FORMULAIRE D'INSCRIPTION"-------------------}}



                       <button class="btn btn-primary" onclick="window.location.href = '{{ route('login') }}';">Se connecter</button>
                    </div>
                    <div class="col-md-7 register-right">
                        <h2><strong>Créer votre compte</strong></h2>
                       <form  method="POST" action="{{route('register')}}">
                           @csrf
                            <div class="register-form">

                                    <div class="form-group">
                                            <label for="Pseudonyme">Pseudonyme</label>
                                                <input  id="Pseudonyme"type=" text" class="form-control @error('Pseudonyme') col-form-label is-invalid @enderror " name="Pseudonyme" value="{{ old('Pseudonyme') }}" required />
                                                @error('Pseudonyme')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                    </div>
                                    <div class="form-group input-group-lg">
                                        <label for="Telephone">Numéro de téléphone</label><br/>
                                        <input id="Telephone" type="tel" class="form-control col-form-label @error('Telephone') is-invalid @enderror"   name="Telephone"  value="{{ old('Telephone') }}" required/>
                                        @error('Telephone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Mot de passe</label>
                                        <input id="password" type="password" class="form-control @error('password') col-form-label is-invalid @enderror" name="password" value="{{ old('Password') }}" required autocomplete="new-password"/>
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="confirm_password">Confirmer le mot de passe</label>
                                        <input id="confirm_password" type="password" class="form-control" name="password_confirmation" value="{{ old('confirm_Password') }}" required autocomplete="new-password"/>
                                    </div>

                                    <button type="submit" class="btn btn-primary">
                                            Créer le compte
                                        </button>

                            </div>
                       </form>

                    </div>
                    </div>
            </div>
        </div>
        </div>
    </div>
    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="build/js/intlTelInput-jquery.min.js"></script>
    <script>
        $("#Telephone").intlTelInput({
            nationalMode:false,
            initialCountry:"tg",


            });

    </script>

</body>

</html>

