<?php
namespace  App\Http\Middleware;


use Closure;

 class NoCacheControlMiddleware
 {

    public function handle($request,Closure $next){

        $response=$next($request);
        /**pas de cache  */
        $response->header('Cache-Control','no-cache,no-store,max-age=0,must re-validate');

        return $response;

    }

 }
