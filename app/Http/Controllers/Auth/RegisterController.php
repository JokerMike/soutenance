<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\Profils;


use Illuminate\Http\Request;
use App\Models\compteUtilisateurs;
use Brick\PhoneNumber\PhoneNumber;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Brick\PhoneNumber\PhoneNumberParseException;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{

        private $number;
        private $codeCountry;

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/wel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */


    protected function validator(array $data)
    {
        $this->number=$data['Telephone'];

        try {
            $num = PhoneNumber::parse($data['Telephone']);
            $this->codeCountry=$num->getRegionCode();
        }
        catch (PhoneNumberParseException $e) {

        }
        return Validator::make($data, [
            'Pseudonyme' => ['required', 'string', 'max:255'],
            'Telephone' => ['required', 'string','max:255', 'unique:compte_utilisateurs'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\compte_utilisateurs
     */
    protected function create(array $data)
    {

        $compte=new compteUtilisateurs;
        $compte->Telephone=$data['Telephone'];
        $compte->Pseudonyme=$data['Pseudonyme'];
        $compte->Code_pays=$this->codeCountry;
        $compte->Password=Hash::make($data['password']);
        $compte->assignRole("user");

        $compte->save();
        //dd("hel");
        return $compte;


    }
}
