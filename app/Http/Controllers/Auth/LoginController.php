<?php

namespace App\Http\Controllers\Auth;

use App\Models\Utilisateurs;
use MercurySeries\Flashy\Flashy;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

     /*public  function authenticated($request,$user){

        if(($user->hasRole("user"))){
                return redirect("/wel");
        }else{
                 //dd("user");
                return redirect("/enregistrement");
             }
     }*/
   protected $redirectTo = '/enregistrement';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function username()
    {
        return 'Telephone';
    }
   protected function guard()
        {
            return Auth::guard('web');
        }
}
