<?php

namespace App\Http\Controllers\API;

use App\Models\Tarif;
use App\Models\Ville;
use App\Models\Agences;
use App\Models\Itineraire;
use App\Models\Utilisateurs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ListTarifController extends Controller
{
    private $agenceId;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $liste=[];

        $user = auth('api')->user();

        $utilisateur=Utilisateurs::whereTelephone($user['Telephone'])->first();

        #trouver l'id de l'agent
       $agent=DB::table('agents')
           ->where('id', $utilisateur->agent)
           ->first();
           $this->agenceId=$agent->agence_id;

        //rechercher toutes les villes de livraison de  l'agence
           $agence=Agences::find($this->agenceId);

           $itineraires=$agence->itineraire()->get();//liste des itineraires de cette agence
         // dd($itineraires);
            //recuperer les informations sur chaque  itineraire enregistres
            for($j=0;$j<count($itineraires);$j++){
              array_push($liste, $this->Info($itineraires[$j]));
            }

        //
        return $liste;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $itineraire=Itineraire::findOrFail($id);
       $result= $this->Info($itineraire);
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function Info($itineraire){
            $listeUnique=[];
            $info=[];
        $tarifVoie=[];
            $infoTarifVoie=[];
            $ville_depart=Ville::find($itineraire->ville_depart);
            $ville_destination=Ville::find($itineraire->ville_destination);

            $tarifs=$itineraire->tarifs;//ensemble des tarifs enregistrés

            for($k=0;$k<count($tarifs);$k++){
                $infoTarifVoie=[];
                //pour chaque tarif enregistre on affiche les info et la voie associés
                $infoTarifVoie=array(
                    'tarif'=>$tarifs[$k],
                    'voie'=>$tarifs[$k]->transport
                );
                array_push($tarifVoie,$infoTarifVoie);
            }

            //infos total
            $info=array(
                'info'=>$tarifVoie,
                'itineraire'=>$itineraire->id,
                'ville_depart'=>$ville_depart->description,
                'ville_destination'=>$ville_destination->description,
              //  'transport'=>$itineraires[$j]->transport
            );
            //array_push($listeUnique,$info);
            return $info;

    }
}
