<?php

namespace App\Http\Controllers\API;

use App\Models\Ville;
use App\Models\Continent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class LocalisationController extends Controller
{
    public function getContinents(){
        return Continent::all();
    }

    public function getCountries($id){

        $pays= DB::table('pays')
        ->where('continent', $id)
        ->get();
        return $pays;
    }

    public function getRegions($id){

        $regions= DB::table('regions')
        ->where('pays', $id)
        ->get();
        return $regions;
    }
    public function getCities($id){

        $villes= DB::table('villes')
        ->where('region', $id)
        ->get();
        return $villes;
    }
    public function search(){
        $villes;
        if($search=\Request::get('q')){

            $villes= DB::table('villes')
            ->where('description','LIKE',"$search%")
            ->get(['id','description']);

            return $villes;

        }
     }
}
