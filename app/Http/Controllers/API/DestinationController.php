<?php

namespace App\Http\Controllers\API;

use App\Models\Tarif;
use App\Models\Ville;
use App\Models\Agences;
use App\Models\Itineraire;
use App\Models\Utilisateurs;
use Illuminate\Http\Request;
use App\Models\transportTypes;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\DestinationRequest;

class DestinationController extends Controller
{
    private $agenceId;
    private $ville;
    /*
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DestinationRequest $request)
    {


           $villeDepart=$request->villeDepart;
           $villeDestination=$request->villeDestination;
           $volume=$request->volume;
           $masse=$request->masse;
           $tarifs= $request->tarifs;

           if($villeDepart==$villeDestination){
             return response()->json([
                'success'=>false,
                'message'=>'les villes ne peuvent etre identiques',

            ]);
           }else{
         //   dd("ok got it again");
               /***
         * recuperer l'agence de l'utilisateur
         */
               $user = auth('api')->user();

               $utilisateur=Utilisateurs::whereTelephone($user['Telephone'])->first();
               #trouver l'id de l'agent
               $agent=DB::table('agents')
           ->where('id', $utilisateur->agent)
           ->first();
           // dd($agent->agence_id);
               $this->agenceId=$agent->agence_id;//id de l'agence

               $itineraireExist=DB::table('itineraires')
               ->whereRaw("((ville_depart=$villeDepart AND ville_destination=$villeDestination ) OR (ville_depart=$villeDestination AND ville_destination=$villeDepart ) )")
                ->where('agences_id',$this->agenceId)->first();

                if($itineraireExist){

                    return response()->json([
                        'success'=>false,
                        'message'=>'itineraire déjà  enregistré par votre agence',

                    ]);
                }else{
                    $agence=Agences::find($this->agenceId);


                    $itineraire=new Itineraire;
                    $itineraire->ville_depart=$villeDepart;
                    $itineraire->ville_destination=$villeDestination;
                    $itineraire->agences_id=$this->agenceId;


                    //creer un itineraire
                    $registerItineaire=$agence->itineraire()->save($itineraire);
                     //dump($registerItineaire);


                    for ($i=0;$i<count($tarifs);$i++) {
                        $tarif=new Tarif();
                        $tarif->montant=$tarifs[$i]['montant'];
                        $tarif->volume=$volume;
                        $tarif->masse=$masse;
                        $tarif->itineraire=$itineraire->id;//id de l'itineraire cree
                        $tarif->duree=$tarifs[$i]['duree'];
                        $voie=$tarifs[$i]['voie'];

                        //relier le tarif a une voie
                        $transport=transportTypes::find($voie);
                        $transport->tarif()->save($tarif);
                    }

                    return response()->json([
                 'success'=>true,
                 'message'=>'tarifs mis a jour',
                    ]);
                }

           }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $itineraire= Itineraire::findOrFail($id);
        $this->validate($request,[
            'tarifs.*.volume'=>'required|numeric',
            'tarifs.*.masse'=>'required|numeric',
            'tarifs.*.voie'=>'required|numeric',
            'tarifs.*.montant'=>'required|numeric',
            'tarifs.*.duree'=>'required|numeric',
        ]);

           $tarifs= $request->tarifs;
            $itineraire->tarifs()->delete();//supprimer tous les tarifs lies a cet itineraire

            //on cree de nouveaux tarifs
         foreach($tarifs as $tarifUnique ){
             $voie=$tarifUnique['voie'];
            $tarif=new Tarif();
            $tarif->montant=$tarifUnique['montant'];
            $tarif->volume=$tarifUnique['volume'];
            $tarif->masse=$tarifUnique['masse'];
            $tarif->itineraire=$itineraire->id;//id de l'itineraire cree
            $tarif->duree=$tarifUnique['duree'];
            $voie=$tarifUnique['voie'];

             $transport=transportTypes::find($voie);
             $transport->tarif()->save($tarif);


         }

        return 'ok';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
