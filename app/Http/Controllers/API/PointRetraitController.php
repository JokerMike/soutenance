<?php

namespace App\Http\Controllers\API;

use App\Models\Ville;
use App\Models\Utilisateurs;
use Illuminate\Http\Request;
use App\Models\PointRetraits;
use App\Models\compteUtilisateurs;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\PointRetraitRequest;

class PointRetraitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PointRetraitRequest $request)
    {
        $user = auth('api')->user();
        //trouver l'id de l'agent
        $agent=compteUtilisateurs::find($user->id);

        //retrouver le compte utilisateur

        $utilisateur=Utilisateurs::whereTelephone($agent->Telephone)->first();
        //trouver l'agence
        $agent=DB::table('agents')
        ->where('id', $utilisateur->agent)
        ->first();

            $point= new PointRetraits();
            $point->nom=$request->nom;
            $point->adresse=$request->addresse;
            $point->telephone=$request->telephone;
            $point->responsable=$request->responsablePoint['agent'];
            $point->agence_id=$agent->agence_id;
            $ville=Ville::findOrFail($request->localisation['ville']);
            $ville->agenceLocalisations()->save($point);
                return 0;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
