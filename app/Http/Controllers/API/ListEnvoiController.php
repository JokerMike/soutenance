<?php

namespace App\Http\Controllers\API;

use App\Models\Listes;
use App\Models\Agences;
use App\Models\Utilisateurs;
use Illuminate\Http\Request;
use App\Models\transportTypes;
use App\Models\compteUtilisateurs;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ListEnvoiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $listeEnvoi=[];
        $listes=Listes::latest()->paginate(10);

        for ($i=0; $i < count($listes); $i++) {
            $infoListe=[];
            $localisationAgence=Agences::findOrfail($listes[$i]->agence_arrivee)->localisation()->first();

            $infoListe=array(
                'liste'=>$listes[$i],
                'localisation'=> $localisationAgence
            );
            array_push($listeEnvoi,$infoListe);
        }
        return $listeEnvoi;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $info=$request->info;
          $user = auth('api')->user();
        //trouver l'id de l'agent
        $agent=compteUtilisateurs::find($user->id);

        //retrouver le compte utilisateur

        $utilisateur=Utilisateurs::whereTelephone($agent->Telephone)->first();
        //trouver l'agence
        $agent=DB::table('agents')
        ->where('id', $utilisateur->agent)
        ->first();

        //verifier que l'agence de depart n'est pas l'agence de destination
        if($agent->agence_id==(int)$info['numero']){
            return ["erreur"=>"agence non conformes"];
        }else{
            return  $liste=Listes::create([
                "codeListe"=>$info['code'],
                "Quantite"=>(int)$info['quantite'],
                "agence_depart"=>$agent->agence_id,
                "agence_arrivee"=>(int)$info['numero'],
                "transport_type_id"=>(int)$info['voie'],
            ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listes= DB::table('listes')
        ->where('transport_type_id', $id)
        ->get();
        return $listes;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return ['ok'=>'hello'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *
     * recuperer les moyens de transport
     *
     * @return void
     */
    public function voie(){

        return transportTypes::all();
    }
    /**
     *
     *
     *
     * @param [type] $id
     *  @return \Illuminate\Http\Response
     */
    public function nom($id){

        $liste=DB::table('listes')
        ->where('id', $id)
        ->first();
        return $liste->codeListe;
    }

}
