<?php

namespace App\Http\Controllers\API;

use App\Models\Ville;
use App\Models\Agents;
use App\Models\Profils;
use App\Models\Utilisateurs;
use Illuminate\Http\Request;
use App\Models\Localisations;
use App\Models\compteUtilisateurs;
use Brick\PhoneNumber\PhoneNumber;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class EmployeController extends Controller
{

    private $agenceId;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $user = auth('api')->user();

        if($user->hasRole('admin')){
           // $this->agenceId='';
        }
        if($user->hasRole('responsable')){
            #retrouver le compte utilisateur
            $utilisateur=Utilisateurs::whereTelephone($user['Telephone'])->first();
             #trouver l'id de l'agent
            $agent=DB::table('agents')
                ->where('id', $utilisateur->agent)
                ->first();
            $this->agenceId=$agent->agence_id;
        }else{
            //$this->agenceId='';
            }

        $identite=$request->identite;
        $localisation=$request->localisation;
        $professionnel=$request->professionnel;
        $tel=$identite['telephone'];
        $num = PhoneNumber::parse($tel);
        $codeCountry=$num->getRegionCode();
        /**creation du compte agent */

      if($codeCountry){
        $agent=Agents::create([
            'Date_embauche'=>$professionnel['dateEmbauche'],
            'agence_id'=>$this->agenceId,
        ]);
        //creation du compte utilisateur

        $user=Utilisateurs::firstOrCreate(
            [
                'Telephone'=>$identite['telephone']
            ],
            [
                'Nom'=>$identite['nom'],
                'Prenom'=>$identite['prenom'],
                'Email'=>$identite['email'],
                'Numero_piece'=>$identite['carteNumber'],
                'Type_piece'=>$identite['typeCarte']
            ]
        );

        $agent->utilisateur()->save($user);

        /**enregistrer la localisation */
        $loc=Ville::findOrFail($localisation['ville']);

            /**Lier la localisation au nouvel agent */
          $user->localisations()->attach($loc);

          /**creer un compte a l'agent */
                $tel=$identite['telephone'];
                $role=$professionnel['role'];
                $password="password";

           // $profil=Profils::find($role);

            $compte=new compteUtilisateurs;
            $compte->Telephone=$identite['telephone'];
            $compte->Pseudonyme=$identite['nom']." ".$identite['prenom'];
            $compte->Code_pays=$codeCountry;
            $compte->Password=Hash::make($password);
            $compte->assignRole($role);
            $compte->save();
            $compte->utilisateur()->save($user);
            return $compte;
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**Recherche d;un nouveau chef d'agence */
     public function search(){
        if($search=\Request::get('q')){
            $chef=Utilisateurs::where(function ($query) use ($search){
                $query->where('Telephone','LIKE',"%$search%")
                      ->orWhere('Telephone','=',"$search");
            })->first();
            return $chef;
        }
     }
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
