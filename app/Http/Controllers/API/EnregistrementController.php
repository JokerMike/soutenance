<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Models\Lot;
use App\Models\Lots;
use App\Models\Colis;
use App\Models\Agences;
use App\Models\Localisation;
use App\Models\Utilisateurs;
use Illuminate\Http\Request;
use App\Models\Localisations;
use App\Models\compteUtilisateurs;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EnregistrementController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    private $colisNombre=0;
     /**
     * Class constructor.
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $enregistrement=[];

        $lots=Lots::latest()->paginate();
        $nombre=0;
        for($i=0;$i<count($lots);$i++){
            $lot=[];
          $expediteur=Utilisateurs::findOrfail($lots[$i]->expediteur);//recuperer l'expediteur du lot
          $destinataire=Utilisateurs::findOrfail($lots[$i]->destinataire);//recuperer le destinataire du lot
         // $localisation=Localisations::findOrfail($lots[$i]->destinataire);
          $lot=array(
              'lot'=>$lots[$i],
              'destinataire'=>$destinataire,
              'expediteur'=>$expediteur,

             // 'localisation'=>$localisation
          );
         // array_push($lot,$lots[$i],,);//enregistrer dans un meme tableau le lot ainsi que son expediteur et on destinataire
          array_push($enregistrement,$lot);//ajouter le lot a la liste d'enregistrement


        }
        return $enregistrement;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return ['response'=>"hello"];

        $expediteur=$request->expediteur;
       $localisation=$request->localisation;
       $destinataire=$request->destinataire;
       $informations=$request->infosColis;
       $identifiant=$request->identifiant;

       /**creer ou trouver l'expediteur */
     $expedi=$this->Expediteur($expediteur,$identifiant);
       /**creer ou trouver le destinataire */
     $destin=Utilisateurs::firstOrCreate(['Telephone'=>$destinataire['telephone']],[
           'Nom'=>$destinataire['nom'],
           'Prenom'=>$destinataire['prenom'],
           'Type_utilisateur'=>"Destinataire"
       ]);
        /**trouver ou enregistrer la localisation */

       $loc=Localisations::firstOrCreate([
           'Continent'=>$localisation['Continent'],
           'Pays'=>$localisation['Pays'],
           'Region'=>$localisation['Region'],
           'Ville'=>$localisation['Ville']
           ]);

           /**Lier la localisation au destinataire */
          $destin->localisations()->attach($loc);

           /**compter le nombre de colis */
           for($i=0;$i<count($informations);$i++){
            $this->colisNombre=$this->colisNombre+$informations[$i]['Quantite'];
           }

           /**Creer le lot*/
      $lot=Lots::create([
        'Quantite'=>$this->colisNombre,
        'expediteur'=>$expedi->id,
        'destinataire'=>$destin->id
       ]);


        /**Enregistrer les colis */
       for($i=0;$i<count($informations);$i++){

            $colis=new Colis;

            $colis->Quantite=$informations[$i]['Quantite'];
            $colis->Detail=$informations[$i]['Detail'];
            $colis->Hauteur=$informations[$i]['Taille'];
            $colis->Masse=$informations[$i]['Masse'];
            $colis->Longueur=$informations[$i]['Longueur'];
            $colis->Largeur=$informations[$i]['Largeur'];
            $lot->colis()->save($colis);
        }

        return $lot;


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth('api')->user();
        //trouver l'id de l'agent
        $agent=compteUtilisateurs::find($user->id);

        //retrouver le compte utilisateur

        $utilisateur=Utilisateurs::whereTelephone($agent->Telephone)->first();
        //trouver l'agence
        $agent=DB::table('agents')
        ->where('id', $utilisateur->agent)
        ->first();

        $lot=Lots::findOrfail($id);
        $colis=$lot->colis()->get();
       /* $colis = DB::table('colis')
                ->where('lot_id', $lot->id)
                ->get();*/

        $expediteur=Utilisateurs::findOrfail($lot->expediteur);
        $localisationExpediteur=Agences::find($agent->agence_id)->localisation()->get();
        $destinataire=Utilisateurs::findOrfail($lot->destinataire);
        $localisation=Utilisateurs::find($lot->destinataire)->localisations()->get();
        $lotInfo=array(
            'colis'=>$colis,
            'lot'=>$lot,
            'destinataire'=>$destinataire,
            'expediteur'=>$expediteur,
           'localisation'=>$localisation,
           'localisationExpediteur'=>$localisationExpediteur
        );

        return $lotInfo;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $info=$request->info;
        $parcolis=$request->parcolis;
        $params=$request->params;
        $lot=$request->lot;
        /*mettre a jour les infos des colis du lot */

        /**rechercher et mettre a jour les infos du lot */
        $l=Lots::findOrFail($lot['id']);


   for ($i=0; $i <count($info); $i++) {
    $this->colisNombre=$this->colisNombre+$info[$i]['Quantite'];
        if(array_key_exists('id',$info[$i])){
            $colis=Colis::findOrFail($info[$i]['id']);
            $colis->update($info[$i]);

        }else{

            $colis=new Colis;
            $colis->Quantite=$info[$i]['Quantite'];
            $colis->Detail=$info[$i]['Detail'];
            $colis->Hauteur=$info[$i]['Hauteur'];
            $colis->Masse=$info[$i]['Masse'];
            $colis->Longueur=$info[$i]['Longueur'];
            $colis->Largeur=$info[$i]['Largeur'];
            $colis->lot_id=$l->id;
            $colis->save();
            $l->colis()->save($colis);
        }


    }

    $lot['liste_id']=$params['liste'];
        $lot['numero_track']=1457;
        $lot['Traite']=true;
        $lot['statut']="enregistrement complet";
        $lot['Quantite']=$this->colisNombre;
        $l->update($lot);



        return $info;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //  \dd("hello");
    }

    /**Rechercher l'utilisateur ou le creer  */
     private function Expediteur($user,$id){

        $expediteur=Utilisateurs::whereTelephone($user['telephone'])->first();

        if($expediteur){
            return $expediteur;
        }else{
            $compte=compteUtilisateurs::find($id);
            $utilisateur= Utilisateurs::create([
                'Nom'=>$user['nom'],
                'Prenom'=>$user['prenom'],
                'Telephone'=>$user['telephone'],
            ]);
           $compte->utilisateur()->save($utilisateur);
           return $compte;
        }

    }

    public function colisretrait($id){
        $colis=Colis::findOrFail($id);
        $colis->delete();
        return ['message' => 'colis supprime'];
    }
}
