<?php

namespace App\Http\Controllers\API;

use App\Models\Ville;
use App\Models\Agences;
use App\Models\Utilisateurs;
use Illuminate\Http\Request;
use App\Models\Localisations;
use App\Http\Controllers\Controller;

class AgenceManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allagences=[];
        $agences=Agences::all();
        foreach($agences as $agence){
                $singleAgence=[];
               $localisation=Ville::whereId($agence['localisation_id'])->first();
               $chef=Utilisateurs::find($agence['agent_chef'])->first();
               $singleAgence=array(
                   'agence'=>$agence,
                   'responsable'=>$chef,
                   'localisation'=>$localisation
               );
               array_push($allagences,$singleAgence);
        }
        return $allagences;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
