<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Models\Utilisateurs;
use Illuminate\Http\Request;
use App\Models\compteUtilisateurs;
use Brick\PhoneNumber\PhoneNumber;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Brick\PhoneNumber\PhoneNumberParseException;

class UserController extends Controller
{
    private $number;
    private $codeCountry;
    public $loginAfterSignUp=true;

    public function register(Request $request){


        $validator = Validator::make($request->all(), [
            'Pseudonyme' => ['required', 'string', 'max:255'],
            'Telephone' => ['required', 'string','max:255', 'unique:compte_utilisateurs'],
            'password' => ['required', 'string', 'confirmed'],
        ]);
            if($validator->fails()){
                return response()->json([
                    'success'=>false,
                    'message'=>"erreur de validation",
                ]);
            }
            $this->validator($request);
        $compte=new compteUtilisateurs;
        $compte->Telephone=$request['Telephone'];
        $compte->Pseudonyme=$request['Pseudonyme'];
        $compte->Code_pays=$this->codeCountry;
        //$compte->Code_pays="TG";
        $compte->Password=Hash::make($request['password']);
        $compte->assignRole("user");
        $compte->save();
        if($this->loginAfterSignUp){
            return $this->login($request);
        }
        return response()->json([
            'success'=>true,
            'status'=>'ok',
            'data'=>$compte,

        ],200);

    }

        protected function validator(Request $request)
        {
            $this->number=$request['Telephone'];

            try {
                $num = PhoneNumber::parse($request['Telephone']);
                $this->codeCountry=$num->getRegionCode();
            }
            catch (PhoneNumberParseException $e) {
                return response()->json([
                    'success'=>false,
                    'message'=>"telephone invalide",
                ]);
            }

        }


    public function login(Request $request){
        $input=$request->only('Telephone','password');
        $jwt_token=null;
        //verifier que le token n'est pas vide
        if(!$jwt_token=JWTAuth::attempt($input)){
                return response()->json([
                        'success'=>false,
                        'message'=>'téléphone ou mot de passe invalide'
                ],401);
        }
        $user=  Auth::user();
        return response()->json([
            'success'=>true,
            'token'=>$jwt_token,
            'user'=>$user
        ],200);


    }
    public function logout(Request $request)
    {
        if(!compteUtilisateurs::checkToken($request)){
            return response()->json([
             'message' => 'Token is required',
             'success' => false,
            ],422);
        }

        try {
            JWTAuth::invalidate(JWTAuth::parseToken($request->token));
            return response()->json([
                'success' => true,
                'message' => 'Deconnection effectuee'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Impossible de se deconnecter'
            ], 500);
        }
    }




}
