<?php

namespace App\Http\Controllers\API;

use App\Models\Ville;
use App\Models\Agents;
use App\Models\Agences;
use App\Models\Itineraire;
use Illuminate\Http\Request;
use App\Models\Localisations;
use App\Models\PointRetraits;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $localisation=$request->localisationAgence;
            $chefAgence=$request->chefAgence;
            $telephone=$request->telAgence;
            $nom=$request->nomAgence;
            $numeroAgent=$chefAgence['id'];

            $agence=new Agences();
            $agence->nom=$nom;
            $agence->Telephone=$telephone;
            $agence->agent_chef=$numeroAgent;

            $loc=Ville::findOrFail($localisation['Ville']);
                $loc->agenceLocalisations()->save($agence);

                $agent=Agents::findorFail($numeroAgent);
                $agent->agence_id=$agence->id;
                $agent->save();
                return $chefAgence;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Fonction pour recuperer les agences
     *
     * @param Request $request
     * @return void
     */
    public function getAgencies(Request $request){
            $agences=[];

        $villeDepart=$request->ville1;
        $villeDestination=$request->ville2;
        $itineraires=DB::table('itineraires')
        ->whereRaw("((ville_depart=$villeDepart AND ville_destination=$villeDestination ) OR (ville_depart=$villeDestination AND ville_destination=$villeDepart ) )")
        ->get();

         //on a trouve des itineraires
        if($itineraires){
           // $agences=$itineraires;
            foreach($itineraires as $itineraire){
                $agence=Agences::findOrFail($itineraire->agences_id);
                $age=array(
                    'agence'=>$agence,
                    'itineraire_id'=>$itineraire

                );
                array_push($agences,$age);
            }
            return response()->json([
                'success'=>true,
                'body'=>$agences,
                   ]);
        }

    }

    public function getAgencyInfo(Request $request){

        $agenceId=$request->id;
        $itineraire=$request->itineraire;
        $agence=Agences::findOrFail($request->id);

        #trouver les points de retraits d'une agence
       // $retraits=DB::table('point_retraits')->where('agence_id ','=',$agenceId)->get();



        $result= $this->Info($itineraire);//recuperer tarif+voie

        # Recuperer toutes les voies des agences
     /*   foreach( $agence->itineraire as $i ){
    $itineraire=
        }*/
        return response()->json([
            'success'=>true,
            'Agences'=>$agence,
           // 'pointRetraits'=>$retraits,
            'tarifs'=>$result
               ]);

    }
        private function Info($id){
            $info=[];
        $tarifVoie=[];

            //$ville_depart=Ville::find($itineraire->ville_depart);
            //$ville_destination=Ville::find($itineraire->ville_destination);
            $itineraire=Itineraire::findOrFail($id);
            $tarifs=$itineraire->tarifs;//ensemble des tarifs enregistrés

            for($k=0;$k<count($tarifs);$k++){
                $infoTarifVoie=[];
                //pour chaque tarif enregistre on affiche les info et la voie associés
                $infoTarifVoie=array(
                    'tarif'=>$tarifs[$k],
                    'voie'=>$tarifs[$k]->transport
                );
                array_push($tarifVoie,$infoTarifVoie);
            }

            //array_push($listeUnique,$info);
            return $tarifVoie;

    }

}
