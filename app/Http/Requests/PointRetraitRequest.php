<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PointRetraitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required',
            'telephone'=>'required|numeric',
            'addresse'=>'required',
            
        ];
    }
    public function messages(){
        return[
            'required' => 'vous devez remplir ce champ',
            'numeric'=>'ce champ doit etre des chiffres'

        ];
    }
}
