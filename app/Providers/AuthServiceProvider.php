<?php

namespace App\Providers;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Model' => 'App\Policies\ModelPolicy',
    ];



    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

       /*  $isEmployee=false;

        Gate::define('isEmployee',function($user){
            dd("Auth::check()");

            $agent= DB::select('select agent from utilisateurs where compte_utilisateur = ?', [$user->id]);
            $administrateur= DB::select('select administrateur from utilisateurs where compte_utilisateur = ?',[$user->id]);

            if($agent||$administrateur){
                $isEmployee=true;
            }
            dd($isEmployee);
            return $isEmployee;

        }); */
/*
        Gate::define('isUser',function($user){
            $agent= DB::select('select agent from utilisateurs where compte_utilisateur = ?', [$user->id]);
            $administrateur= DB::select('select administrateur from utilisateurs where compte_utilisateur = ?',[$user->id]);
            if(!$agent && !$administrateur){
                $isEmployee=true;
            }
            return $isEmployee;
        }); */
        Passport::routes();

        //
    }
}
