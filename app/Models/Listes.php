<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Listes extends Model
{
    protected $fillable=['codeListe','Quantite','Traite','Receptionne','agence_depart','agence_arrivee','transport_type_id'];
}
