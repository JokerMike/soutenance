<?php

namespace App\Models;
use Illuminate\Notifications\Notifiable;
 use Illuminate\Foundation\Auth\User as Authenticatable;


use Illuminate\Database\Eloquent\Model;

class compteAdmin  extends Authenticatable
{
    use Notifiable;

    protected $guard="admin";
    protected $fillable=['Telephone','Password','profil_id','Code_Pays'];

    protected $hidden=['Password','Telephone','profil_id'];


}
