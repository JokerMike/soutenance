<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pays extends Model
{
    protected $fillable=['id','nom','codePays'];

    public function Region(){
        return $this->hasMany('App\Models\Region','id');
    }
    public function Continent(){
        return $this->belongsTo('App\Models\Continent');
    }
}
