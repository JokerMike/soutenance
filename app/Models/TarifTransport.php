<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TarifTransport extends Model
{
    protected $fillable=['id','transport_id','tarif_id'];
}
