<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Localisations extends Model
{
    protected $fillable=['Continent','Pays','Region','Quartier','Ville','Rue'];

    public function utilisateurs(){
        return $this->belongsToMany('App\Models\Utilisateurs','localisation_utilisateurs');
    }
    public function agenceLocalisations(){
        return $this->hasMany('App\Models\Agences','localisation_id');
    }
}
