<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Colis extends Model
{
    protected $fillable=['Longueur','Largeur','Masse','Hauteur','Quantite','Detail','lot_id'];

    public function lots(){
        return $this->belongsTo('App\Models\Lots');
    }

}
