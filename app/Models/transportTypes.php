<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class transportTypes extends Model
{
    protected $fillable=['Description','id'];
    protected $hidden=['created_at','updated_at','pivot'];
    public function tarif(){
        return $this->belongsToMany('App\Models\Tarif','tarif_transports','transport_id','tarif_id');
    }

}
