<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lots extends Model
{
    protected $fillable=['Quantite','numero_track','expediteur','destinataire','traite','statut','liste_id','livraison_id','itineraire_id'];
    public function Expediteur(){
        return $this->belongsTo('App\Models\Utilisateurs');
    }
    public function Destinataire(){
        return $this->belongsTo('App\Models\Utilisateurs');
    }
    public function colis(){
        return $this->hasMany('App\Models\Colis','lot_id');
    }

}
