<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Itineraire extends Model
{
    protected $hidden=['ville_depart','ville_destination'];


    public function agence(){
        return $this->belongsTo('App\Models\Agences','agences_id');
    }
    public function tarifs(){
        return $this->hasMany('App\Models\Tarif','itineraire');
    }
}
