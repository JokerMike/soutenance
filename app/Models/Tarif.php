<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tarif extends Model
{
    protected $fillable=['montant','id','masse','volume','duree','itineraire'];
    protected $hidden=['created_at','updated_at','transport'];

public function itineraire(){
    return $this->belongsTo('App\Models\Itineraire');
}

public function transport(){
    return $this->belongsToMany('App\Models\transportTypes','tarif_transports','tarif_id','transport_id');
}

}
