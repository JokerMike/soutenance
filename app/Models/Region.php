<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable=['id','nom','pays'];

    public function ville(){
        return $this->hasMany('App\Models\Ville','id');
    }
    public function Pays(){
        return $this->belongsTo('App\Models\Pays');
    }
}
