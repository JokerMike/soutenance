<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class permissionsProfil extends Model
{
    protected $fillable=['permissions_id','profils_id'];
}
