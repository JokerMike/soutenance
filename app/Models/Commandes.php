<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commandes extends Model
{
    protected $fillable=['Article','Quantite','Lien_commande','agent_id','utilisateur_id'];
    protected $hidden=['agent_id','utilisateur_id'];
}
