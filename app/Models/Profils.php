<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profils extends Model
{
    protected $fillable=['Description'];

    public function permissions(){
        return $this->belongsToMany('App\Models\Permissions','permissions_profils');
    }
    public function compte_utilisateur(){
        return $this->hasMany('App\Models\compteUtilisateurs');
    }

}
