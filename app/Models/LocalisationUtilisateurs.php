<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocalisationUtilisateurs extends Model
{
    protected $fillable=['ville_id','utilisateurs_id'];
}
