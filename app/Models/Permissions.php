<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    protected $fillable=['Description','Slug'];

    public function profils(){
        return $this->belongsToMany('App\Models\Profils'.'permissions_profils');
    }
}
