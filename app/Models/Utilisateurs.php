<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Utilisateurs extends Model
{

    protected $fillable=['Nom','Prenom','Email','Telephone','BP','compte_utilisateur','administrateur','agent'];

    public function compte(){
        return $this->belongsTo('App\Models\compteUtilisateurs','compte_utilisateur');
    }
    public function localisations(){
        return $this->belongsToMany('App\Models\Ville','localisation_utilisateurs');
    }
    public function Expediteur(){
        return $this->hasMany('App\Models\Lots');
    }
    public function Destinataire(){
        return $this->hasMany('App\Models\Lots');
    }
    public function agent(){
        return $this->belongsTo('App\Models\Agents');
    }
    /*public  function isAdmin(){
        return $this->administrateur;
    }*/
   /* public  function isAgent(){
        return $this->agent;
    }*/
}
