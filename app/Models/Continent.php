<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Continent extends Model
{
    protected $fillable=['id','Description'];

    public function Pays(){
        return $this->hasMany('App\Models\Pays','id');
    }
}
