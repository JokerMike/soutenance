<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agents extends Model
{
    protected $fillable=['Date_embauche','agence_id','responsable_agence'];
    public function utilisateur(){
        return $this->hasOne("App\Models\Utilisateurs",'agent');
    }
    public function ResponsableDe(){
        return $this->hasOne("App\Models\Agence",'responsable_agence');
    }
}
