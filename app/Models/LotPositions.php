<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LotPositions extends Model
{
    protected $fillable=['positions_id','lots_id'];
}
