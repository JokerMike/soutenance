<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ville extends Model
{
    protected $fillable=['id','nom','region'];

    public function utilisateurs(){
        return $this->belongsToMany('App\Models\Utilisateurs','localisation_utilisateurs');
    }

    public function agenceLocalisations(){
        return $this->hasMany('App\Models\Ville','localisation_id');
    }

    public function Region(){
        return $this->belongsTo('App\Models\Region');
    }
    public function tarif()
    {
        return $this->morphToMany('App\Models\Tarif','taggables','taggables','taggable_id','id');
    }
}
