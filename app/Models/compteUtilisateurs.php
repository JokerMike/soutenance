<?php

namespace App\Models;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class compteUtilisateurs extends Authenticatable implements JWTSubject
{
    use HasRoles;
    use HasApiTokens, Notifiable;

    public $timestamps = false;

    protected $table = 'compte_utilisateurs';

    protected $fillable=['Telephone','Password','profils_id','Code_pays','Pseudonyme'];

    protected $hidden=['Password'];

    public function getAuthPassword()
    {
      return $this->Password;

    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function checkToken($token){
        if($token->token){
            return true;
        }
        return false;
    }


    public function profils()
    {
        return $this->belongsTo('App\Models\Profils');
    }
    public function utilisateur(){
        return $this->hasOne('App\Models\Utilisateurs','compte_utilisateur');
    }

}
