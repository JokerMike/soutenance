<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reclamations extends Model
{
    protected $fillable=['Commentaires','Traite'];
}
