<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agences extends Model
{
    protected $fillable=['Telephone','localisation_id','agent_chef','nom'];

    public function agent_chef(){
        return $this->belongsTo('App\Models\Agents');
    }
    public function localisation(){
        return $this->belongsTo('App\Models\Localisations','localisation_id');
    }
    public function itineraire(){
        return $this->hasMany('App\Models\Itineraire');
    }
    public function pointRetraits(){
       return $this->hasMany('App\Models\PointRetraits','id');
    }
}
