<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources([
    'commande'=>'API\CommandeController',
    'Enregistrement'=>'API\EnregistrementController',
    'ListEnvoi'=>'API\ListEnvoiController',
    'Permission'=>'API\PermissionController',
    'Profil'=>'API\ProfilController',
    'barcode'=>'API\barcodeController',
    'Qrcode'=>'API\QrcodeController',
    'Employe'=>'API\EmployeController',
    'Agence'=>'API\AgencyController',
    'AgenceManagement'=>'API\AgenceManagementController',
    'user'=>'API\UserController',
    'destination'=>'API\DestinationController',
    'pointRetrait'=>'API\PointRetraitController',
    'listTarif'=>'API\ListTarifController',


]);

Route::get('/utilisateurCourant/{id}','API\CurrentUserController@getUserInformations')->middleware('api');
Route::get('/findUser','API\EmployeController@search');
Route::get('/voie','API\ListEnvoiController@voie')->middleware('api');
Route::get('/voie/{id}','API\ListEnvoiController@show')->middleware('api');
Route::get('/listeCode/{id}','API\ListEnvoiController@nom')->middleware('api');
Route::get('/enregistrement/{id}','API\EnregistrementController@show')->middleware('api');
Route::post('/colisretrait/{id}','API\EnregistrementController@colisretrait')->middleware('api');
Route::post( 'userRegister','API\UserController@register');
Route::post( 'userlogin','API\UserController@login');
Route::get( 'userlogout','API\UserController@logout');
Route::get( 'continent','API\LocalisationController@getContinents');
Route::get( 'continent/{id}','API\LocalisationController@getCountries');
Route::get( 'pays/{id}','API\LocalisationController@getRegions');
Route::get( 'region/{id}','API\LocalisationController@getCities');
Route::get('all','API\LocalisationController@search');
Route::post('agences','API\AgencyController@getAgencies');
Route::post('destination','API\DestinationController@store');
Route::post('/agencyInfos','API\AgencyController@getAgencyInfo');
